document.getElementById('playBtn').addEventListener('click', onPlay);

const filePicker = document.getElementById('filePicker');
const header = document.getElementById('header');
const yellow_lane = document.getElementById('yellowLane');
const red_lane = document.getElementById('redLane');
const green_lane = document.getElementById('greenLane');
const blue_lane = document.getElementById('blueLane');
const black_lane = document.getElementById('blackLane');

const anim = [
  { opacity: 0, top: '0px' },
  { fontSize: '24px', offset: 0 },
  { opacity: 1, offset: 0.1 },
  { fontSize: '24px', offset: 0.91 },
  { textShadow: '0 0 0', offset: 0.92 },
  { opacity: 1, offset: 0.92 },
  //{ textShadow: '0 1px 15px white', offset: 0.92 },
  { fontSize: '28px', offset: 0.92 },
  { textShadow: '0 0 20px #ffffff, 0 0 30px #ffffff, 0 0 40px #ffffff', offset: 0.93 },
  { textShadow: '0 0 30px #ffffff, 0 0 40px #ffffff, 0 0 50px #ffffff', offset: 0.95 },
  { opacity: 0, top: "580px"}
];

const animTiming = {
  duration: 5000,
  easing: "linear",
  fill: "forwards"
}

const notes = {
  'A4': { elm: yellow_lane, cls: ['yellow', 'left'], chr: '&#x25C0;'},
  'E5': { elm: yellow_lane, cls: ['yellow', 'center'], chr: '&#x2B24;'},
  'B5': { elm: yellow_lane, cls: ['yellow', 'right'], chr: '&#x25B6;'},
  'B4': { elm: red_lane, cls: ['red', 'left'], chr: '&#x25C0;'},
  'F5': { elm: red_lane, cls: ['red', 'center'], chr: '&#x2B24;'},
  'C6': { elm: red_lane, cls: ['red', 'right'], chr: '&#x25B6;'},
  'C5': { elm: green_lane, cls: ['green', 'left'], chr: '&#x25C0;'},
  'A5': { elm: green_lane, cls: ['green', 'center'], chr: '&#x2B24;'},
  'D6': { elm: green_lane, cls: ['green', 'right'], chr: '&#x25B6;'},
  'D5': { elm: blue_lane, cls: ['blue', 'left'], chr: '&#x25C0;'},
  'G5': { elm: blue_lane, cls: ['blue', 'center'], chr: '&#x2B24;'},
  'E6': { elm: blue_lane, cls: ['blue', 'right'], chr: '&#x25B6;'},
  'C1': { elm: black_lane, cls: ['white', 'left'], chr: '&#x25FC;'},
  'G7': { elm: black_lane, cls: ['white', 'right'], chr: '&#x25BC;'},
};

function createNote(noteName) {
  if (!noteName) {
    return;
  } 
  const obj = notes[noteName];
  if (!obj) {
    return;
  }
  const p = document.createElement("p");
  p.classList.add('symbol');
  p.classList.add(...obj.cls);
  p.innerHTML = obj.chr; 
  obj.elm.appendChild(p);
  p.animate(anim, animTiming);
}

function onPlay() {
  if (filePicker.files.length < 1) {
    return;
  }
  const Player = new MidiPlayer.Player((event) => {  
    if (event.name === 'Note on') {
      createNote(event.noteName);
    }
  });
  const file = filePicker.files[0];
  const fr = new FileReader();
  const data = file.arrayBuffer().then((x) => {
    Player.loadArrayBuffer(x);
    Player.play();
    header.style.visibility = "hidden";
  });
}

